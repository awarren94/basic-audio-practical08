//
//  SquareOscillator.cpp
//  JuceBasicAudio
//
//  Created by Andrew Warren on 13/11/2015.
//
//

#include "SquareOscillator.h"
#include "Oscillator.h"
#include <cmath>

float SquareOscillator::renderWaveShape(const float currentPhase)
{
    
    float amplitude;
    for (int i = 0; i < 100; i++) {
        if (currentPhase <= M_PI)
        {
            amplitude = 1.0;
        }
        else
        {
            amplitude = -1.0;
        }
    }
    return currentPhase * amplitude;
    
}