//
//  SquareOscillator.h
//  JuceBasicAudio
//
//  Created by Andrew Warren on 13/11/2015.
//
//

#ifndef H_SQUAREOSCILLATOR
#define H_SQUAREOSCILLATOR

#include <stdio.h>
#include "Oscillator.h"


class SquareOscillator: public Oscillator
{
public:

    float renderWaveShape(const float currentPhase) override;
    
private:
    float frequency;
};
#endif /* H_SQUAREOSCILLATOR */
