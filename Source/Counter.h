//
//  Counter.h
//  JuceBasicAudio
//
//  Created by Andrew Warren on 11/11/2015.
//
//

#ifndef COUNTER_H_INCLUDED
#define COUNTER_H_INCLUDED

#include "JuceHeader.h"

class Counter: public Thread
{
public:
    
    Counter();
    
    ~Counter();
    
    void run() override;
    
    void startMyThread();
    
    void stopMyThread(float ms);
    
    /** Class for counter listeners to inherit */
    class Listener
    {
    public:
        /** Destructor. */
        virtual ~Listener() {}
        /** Called when the next timer has reached the next interval. */
        virtual void counterChanged (const unsigned int counterValue) = 0;
    };
    
    void addListener (Listener* newListener);
    
    void setInterval (int bpm);
    
    
private:
    Listener* listener;
    int counterValue;
    uint32 startTime;
    uint32 interval;
};




#endif /* COUNTER_H_INCLUDED */
